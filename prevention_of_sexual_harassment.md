# Sexual Harassment Awareness

## What kinds of behavior cause sexual harassment?

Sexual harassment can encompass a wide range of behaviors that are unwelcome and of a sexual nature. Some common examples include:

1. **Unwanted Advances:** Making sexual advances or propositions toward someone without their consent.
2. **Sexual Comments or Jokes:** Making inappropriate comments, jokes, or gestures of a sexual nature.
3. **Unwelcome Touching:** Touching someone in a sexual manner without their consent, such as groping or unwanted physical contact.
4. **Sexual Coercion:** Using power or authority to pressure someone into sexual activity.
5. **Sexual Assault:** Any form of unwanted sexual contact or activity, including rape and attempted rape.

It's important to note that sexual harassment can occur in various settings, including the workplace, educational institutions, public spaces, and online environments.

## What to do if you face or witness any incident of such behavior?

If you face or witness any incident or repeated incidents of sexual harassment, it's crucial to take action to address the situation and support those affected. Here are some steps you can take:

1. **Document the Incident:** Write down the details of the incident, including what happened, when and where it occurred, and any witnesses present.
2. **Report the Incident:** If you feel safe and comfortable doing so, report the incident to a trusted authority figure or organization, such as a supervisor, human resources department, school official, or law enforcement.
3. **Seek Support:** Reach out to friends, family members, or support organizations for emotional support and guidance.
4. **Know Your Rights:** Familiarize yourself with your rights regarding sexual harassment and discrimination, both in your workplace or educational institution and under the law.
5. **Take Care of Yourself:** Practice self-care and prioritize your well-being during this challenging time. Consider seeking counseling or therapy if needed.

Remember, you are not alone, and there are resources and support available to help you navigate and address incidents of sexual harassment.

