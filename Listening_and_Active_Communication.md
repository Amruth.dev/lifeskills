# Listening and Active Communication

## What are the steps/strategies to do Active Listening?

- Avoid getting distracted by our own thoughts.
- don't interrupt let them finish and then respond.
- use door openers such as "sounds interesting", "I am listening" etc.
- Show that you are listening with body language.
- Take notes during important conversations if necessary.

## According to Fisher's model, what are the key points of Reflective Listening?

- **Active Engagement**: Fully engage with the speaker without interrupting.
  
- **Understanding and Empathy**: Aim to understand the speaker's perspective and feelings.

- **Paraphrasing**: Summarize or paraphrase the speaker's message to ensure understanding.

- **Validation**: Acknowledge the speaker's feelings and thoughts without judgment.

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

## What are the obstacles in your listening process?

- **Too Many Noises**: Sometimes, there are too many noises around to hear well.

- **Thinking Other Things**: Sometimes, we think about other things and miss what's being said.

- **Feeling Upset**: When we're upset, it's hard to focus on what someone else is saying.

- **Not Paying Attention**: Sometimes, we're not really listening because we're not paying attention.

## What can you do to improve your listening?

- **Quiet Place**: Find a quiet spot where there aren't many noises to distract you.

- **Look and Listen**: Pay attention to the person speaking and look at them when they talk.

- **Ask Questions**: If you don't understand something then ask questions to learn more.

- **Practice**: Try listening more when people talk, and it will get easier over time.

## When do you switch into Aggressive communication styles in your day to day life?

- **Expressing Anger**: we can switch to aggressive communication when we express anger

- **Dominating Conversations**: when you want to dominate the current conversation.

- **Ignoring Feelings**: You might dismiss or invalidate others' feelings or opinions.

## When do you switch into Passive Aggressive communication styles in your day to day life?

- while using sarcasm.
- while Gossiping about some topics.
- while Making subtle, snide remarks to irritate or provoke others.

## How can you make your communication assertive?

- Clearly express your thoughts, feelings, and needs without being aggressive or passive.

- Speak from your own perspective using "I feel" or "I think" to own your feelings and opinions.

- Pay attention to others' viewpoints and respond thoughtfully without interrupting.

- Clearly communicate your limits and expectations to maintain respect and mutual understanding.
