# Grit and Growth Mindset

### 1. Summarize the video in a few (1 or 2) lines. Use your own words

- Grit involves persevering through challenges and refusing to quit, even when faced with adversity, to pursue your objectives.

---

### 2. Summarize the video in a few (1 or 2) lines in your own words

- Having a growth mindset is all about believing you can get better if you keep at it and don't throw in the towel. It's about having faith in yourself and learning from your slip-ups.

---

### 3. What is the Internal Locus of Control? What is the key point in the video?

- The internal locus of control is feeling like you have a say in what happens in your life. The video highlights the importance of believing that you can shape your own future.

---

### 4. What are the key points mentioned by the speaker to build a growth mindset

To build a growth mindset:

- Learn from your mistakes.
- Take inspiration from others' success.
- Stay strong when things don't pan out as planned.
- Trust in the power of getting better.

---

### 5. What are your ideas to take action and build a Growth Mindset?

My plan to build a growth mindset:

- I'll make sure I understand each idea properly.
- I'll stay cool and focused, no matter what comes my way.
- I'll take full responsibility for my learning.
- Instead of saying "I can't," I'll switch it up to "I'll give it a shot."

---
