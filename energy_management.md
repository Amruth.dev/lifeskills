# Energy-management

### 1. What are the activities you do that make you relax - Calm quadrant?

- Playing Cricket with friends.
- Listening to music.
- spend time with family.

---

### 2. When do you find getting into the Stress quadrant?

- Dealing with unexpected problems or conflicts.
- Taking on too many responsibilities at once.
- Approaching deadlines.

---

### 3.  How do you understand if you are in the Excitement quadrant?

- I feel energized and eager about upcoming opportunities.
- I view challenges as adventures rather than obstacles.
- I am inspired and motivated to take on new tasks or projects.

---

### 4. Paraphrase the Sleep is your Superpower video in your own words in brief?

- Sleep can reduce anxiety.
- Adequate sleep contributes to reducing heart problems.
- Sleep increases our life span.
- Sleep boosts creativity.

---

### 5. What are some ideas that you can implement to sleep better?

- Stick to a consistent sleep schedule.
- Develop a relaxing bedtime routine.
- Create a comfortable sleep environment.
- Limit screen time before bed

---

### 6. Paraphrase the video - Brain Changing Benefits of Exercise?

- Exercise makes our brain better.
- Exercise gives good stuff to our brain.
- Exercise helps our brain grow.
- Exercise makes you smarter.
- Exercise keeps our brain strong.

---

### 7. What are some steps you can take to exercise more?

- Start small and gradually increase activity.
- Choose activities you enjoy.
- Schedule exercise like any other appointment.
- Mix up your routine to stay engaged.
- Find a workout buddy for accountability.

---
